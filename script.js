$(document).ready(function () {
  var apiAddress = 'https://iths.bbweb.se/articles';
  var apiKey = '?key=a4b0ef87-54f5-4e15-9374-a52b11c68991';
  var $menu = $('.menu-left');
  var $content = $('.content');
  var $articleTitle = $('.article-title');
  var $articleAuthor = $('.article-author');
  var $articleDate = $('.article-date');
  var $articleText = $('.article-text');
  
  function createButtonsFromArray(array) {
    for (var i = 0, len = array.length; i < len; i++) {
      $menu.append('<button>' + array[i]['title'] + '</button>');
      $('.menu-left button').last().on('click', getArticle(array[i]['_id']));
    }
  }
  
  function noArticle() {
    $('.content button').remove();
    $articleTitle.text('No article');
    $articleAuthor.text('Författare: ');
    $articleDate.text('Datum: ');
    $articleText.text('Please select an article from the left-hand menu');
    $('button').removeClass('bold');
  }
  
  function getArticle(articleId) {
    return function () {
      noArticle();
      $(this).addClass('bold');
      if (articleId !== undefined) {
        $articleText.text('Laddar...');
        $.getJSON((apiAddress + '/' + articleId + apiKey), function (data) {
          setArticle(data);
        });
      }
    }
  }
  
  function setArticle(article) {
    $articleTitle.text(article['title']);
    $articleAuthor.append('<span class="cursive"><a href="mailto:' + article['authorEmail'] + '">' + article['author'] + '</a></span>');
    $articleDate.append('<span class="cursive">' + article['date'] + '</span>');
    $articleText.text('');
    $articleText.append(article['text']);
    $content.append('<button>Ta bort</button>');
    $('.content button').on('click', deleteArticle(article['_id']));
  }
  
  function deleteArticle(articleId) {
    return function () {
      $.ajax((apiAddress + '/' + articleId + apiKey), {
        type: 'DELETE',
        success: function() {
          loadMenu();
        }
      });
    }
  }
  
  function loadMenu() {
    $menu.empty();
    noArticle();
    $menu.append('<button>No article</button>');
    $('.menu-left button').last().on('click', getArticle()).addClass('bold');
    $.getJSON((apiAddress + apiKey), function (data) {
      createButtonsFromArray(data);
    });
  }
  
  loadMenu();
});
